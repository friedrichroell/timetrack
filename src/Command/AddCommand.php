<?php

namespace TimeTrack\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use TimeTrack\Manager;
use TimeTrack\Report;
use TimeTrack\Section;
use TimeTrack\Session;

class AddCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('add')
            ->setDescription('Add a section to a Session')
            ->addArgument(
                'reportName',
                InputArgument::OPTIONAL,
                'The name of your section you want to track'
            )
            ->addArgument(
                'length',
                InputArgument::OPTIONAL,
                'The length in minutes, if not given, the remaining time will be used'
            )
            ->addArgument(
                'identifier',
                InputArgument::OPTIONAL,
                'The timepoint of the session. e.g. yesterday'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = new Manager();

        $session = $manager->load($input->getArgument('identifier'));

        if ( ! $reportName = $input->getArgument('reportName')) {
            $reportName = $this->askForReportName($input, $output, $session);
        }

        $length = (int) $input->getArgument('length');

        if ($length < 0) {
            $length = $session->getUntrackedTimeLength() + $length * 60;
        } else if ($length > 0) {
            $length = $length * 60;
        } else {
            $length = $session->getUntrackedTimeLength();
        }

        $section = new Section($length, new Report($reportName));

        $session->add($section);

        $manager->save($session);

        $command = $this->getApplication()->find('report');
        $command->run(new ArrayInput([]), $output);
    }

    private function askForReportName(InputInterface $input, OutputInterface $output, Session $session)
    {
        $helper = $this->getHelper('question');
        $question = new Question('Enter report name: ');
        $question->setAutocompleterValues($session->getReportNames());

        return $helper->ask($input, $output, $question);
    }
}
