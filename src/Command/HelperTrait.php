<?php

namespace TimeTrack\Command;

use Symfony\Component\Console\Output\OutputInterface;
use TimeTrack\Session;

trait HelperTrait
{
    private function checkSession($session, OutputInterface $output, $identifier = '')
    {
        if ($session instanceof Session) {
            return true;
        }

        $output->writeln(sprintf('<comment>No session for <error>%s</error> in storage</comment>', date('l, d.m.Y', strtotime($identifier . ' midnight'))));

        return false;
    }
}