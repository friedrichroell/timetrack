<?php

namespace TimeTrack\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TimeTrack\Manager;

class RemoveCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('remove')
            ->setDescription('Remove a section from a Session')
            ->addArgument(
                'reportName',
                InputArgument::REQUIRED,
                'The name of your section you want to remove'
            )
            ->addArgument(
                'identifier',
                InputArgument::OPTIONAL,
                'The timepoint of the session. e.g. yesterday'
            )
            ->addOption(
                'id',
                'i',
                InputOption::VALUE_NONE
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = new Manager();
        $session = $manager->load($input->getArgument('identifier'));

        if ($input->getOption('id')) {
            $session->removeById((int) $input->getArgument('reportName'));
        } else {
            $session->remove($input->getArgument('reportName'));
        }


        $manager->save($session);

        $command = $this->getApplication()->find('report');
        $command->run(new ArrayInput([]), $output);
    }
}
