<?php

namespace TimeTrack\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TimeTrack\Manager;
use TimeTrack\Session;


class ReportCommand extends Command
{
    use HelperTrait;

    protected function configure()
    {
        $this
            ->setName('report')
            ->setDescription('Prints the content of the session')
            ->addArgument(
                'identifier',
                InputArgument::OPTIONAL,
                'The timePoint of the session. e.g. yesterday'
            )
            ->addOption(
                'grouped',
                'g',
                InputOption::VALUE_NONE
            )
            ->addOption(
                'maskOffTime',
                'm',
                InputOption::VALUE_NONE,
                'hide tracked times, that are marked as PAUSE or MITTAG'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $maskOffTime = $input->getOption('maskOffTime');
        $identifier = $input->getArgument('identifier');

        $manager = new Manager();

        if (! $this->checkSession($session = $manager->load($identifier), $output, $identifier)) {
            return;
        }

        $output->writeln('');
        $output->writeln(sprintf('<info>Session %s</info>', date('d.m.Y', $session->getIdentifier())));

        $table = new Table($output);

        if ($input->getOption('grouped')) {
            $table
                ->setHeaders(array('Name', 'Length', 'Share'))
                ->setRows($session->getDataGrouped($maskOffTime));
        } else {
            $table
                ->setHeaders(array('ID', 'From', 'To', 'Name', 'Length'))
                ->setRows($session->getData($maskOffTime));
        }

        $table->render();
        $output->writeln('<info>Total ' . $session->getHumanLength($maskOffTime) . '</info>');
        $output->writeln('');
    }
}