<?php

namespace TimeTrack\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TimeTrack\Manager;

class SectionEditTimeCommand extends Command
{
    use HelperTrait;

    protected function configure()
    {
        $this
            ->setName('section:edit-time')
            ->setDescription('Add a section to a Session')
            ->addArgument(
                'id',
                InputArgument::REQUIRED
            )
            ->addArgument(
                'newTime',
                InputArgument::REQUIRED
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = new Manager();

        if (! $this->checkSession($session = $manager->load(), $output)) {
            return;
        }

        if ( ! ($section = $session->getSectionById($input->getArgument('id')))) {
            $output->writeln('invalid section id');
            return;
        }

        $section->setLength($input->getArgument('newTime') * 60);
        $manager->save($session);

        $command = $this->getApplication()->find('report');
        $command->run(new ArrayInput([]), $output);
    }
}