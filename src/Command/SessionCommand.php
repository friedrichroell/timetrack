<?php

namespace TimeTrack\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TimeTrack\Manager;
use TimeTrack\Session;


class SessionCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('session')
            ->setDescription('Add a section to a Session')
            ->addArgument(
                'action',
                InputArgument::REQUIRED,
                'What do you want to do? [open|show|set-start]'
            )
            ->addArgument(
                'beginning',
                InputArgument::OPTIONAL,
                'Set the start time for a session'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = new Manager();

        switch ($input->getArgument('action')) {
            case 'open':
                $manager->save(new Session());
                $output->writeln('New Session opened!');
                return null;
            case 'set-start':
                return $this->setStartTime($input, $output);
            case 'show':
                return $this->showSessions($input, $output);
        }

        $output->writeln('Invalid action!');
    }

    private function setStartTime(InputInterface $input, OutputInterface $output)
    {
        if ( ! $beginning = $input->getArgument('beginning')) {
            $output->writeln('You need to actually give me a time, please');
            return;
        }

        if (0 === ($stamp = strtotime($beginning))) {
            $output->writeln('Given time is invalid');
            return;
        }

        if (date('dmY') !== date('dmY', $stamp)) {
            $output->writeln('The given time needs to be on the same day as the sessions');
            return;
        }

        $manager = new Manager();
        $session = $manager->load();
        $session->setBeginning($stamp);
        $manager->save($session);
    }

    private function showSessions(InputInterface $input, OutputInterface $output)
    {
        $sessions = (new Manager())->loadAll();

        $table = new Table($output);

        $table->setHeaders(['Date', 'Day', 'Sessions', 'Work Time', 'Total Time']);

        foreach ($sessions as $session) {
            $table->addRow([
                $session->getDate()->format('d.m.Y'),
                $session->getDate()->format('l'),
                $session->count(),
                $session->getHumanLength(true),
                $session->getHumanLength(false),
            ]);
        }

        $table->render();
    }
}
