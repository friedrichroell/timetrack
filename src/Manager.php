<?php

namespace TimeTrack;

use Flintstone\Flintstone;
use Flintstone\Formatter\JsonFormatter;

class Manager
{
    const RELATIVE_FILE_STORAGE_PATH = '/../var/fileStorage';

    /**
     * @var Flintstone
     */
    private $database;

    public function __construct()
    {
        $this->initDatabase();
    }

    /**
     * @param string $identifier
     *
     * @return Session
     */
    public function load($identifier = null)
    {
        $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));

        if (null !== $identifier) {
            $date->modify($identifier);
        }

        return  Session::create($this->database->get($date->setTime(0, 0, 0)->getTimestamp()));
    }

    /**
     * @return Session[]
     */
    public function loadAll()
    {
        $result = [];

        foreach ($this->database->getAll() as $session) {
            $result[] = Session::create($session);
        }

        return $result;
    }

    public function save(Session $session)
    {
        $this->database->set($session->getIdentifier(), $session->toString());
    }

    private function initDatabase()
    {
        $options = [
            'dir' => __DIR__ . self::RELATIVE_FILE_STORAGE_PATH,
            'formatter' => new JsonFormatter(),
        ];

        $this->database = new Flintstone('Session', $options);
    }
}
