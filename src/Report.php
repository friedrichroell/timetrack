<?php

namespace TimeTrack;

class Report
{
    private $title;

    private static $offTimeStrings = [
        'PAUSE',
        'MITTAG',
    ];

    public function __construct($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function isOffTime()
    {
        return in_array($this->title, self::$offTimeStrings);
    }
}