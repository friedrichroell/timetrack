<?php

namespace TimeTrack;

use DateTime;
use Exception;

class Section
{
    private $length;

    /**
     * @var Report
     */
    private $report;

    public function __construct($length, Report $report)
    {
        if ( ! is_integer($length)) {
            throw new Exception('$length needs to be of type int');
        }

        $this->length = $length;
        $this->report = $report;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    public function getHumanLength()
    {
        $dtF = new DateTime('@0');
        $dtT = new DateTime('@' . $this->getLength());

        return $dtF->diff($dtT)->format('%H:%I:%S');
    }

    /**
     * @param int $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param Report $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }
}
