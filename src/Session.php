<?php

namespace TimeTrack;

use DateTime;

class Session
{
    /**
     * @var int TimeStamp
     */
    private $beginning;

    /**
     * @var Section[]
     */
    private $sections;

    private $identifier;

    public function __construct($beginning = null)
    {
        $this->beginning = is_null($beginning) ? time() : $beginning;
        $this->sections = [];
        $this->identifier = (new \DateTime())
            ->setTimezone(new \DateTimeZone('UTC'))
            ->setTime(0, 0, 0)
            ->getTimestamp();
    }

    public function add(Section $section)
    {
        $this->sections[] = $section;
    }

    public function removeById($id)
    {
        $filtered = array_filter($this->sections, function(int $key) use ($id) {
            return $key !== $id;
        }, ARRAY_FILTER_USE_KEY);

        $this->sections = array_values($filtered);
    }

    public function remove($name)
    {
        $filtered = array_filter($this->sections, function(Section $section) use ($name) {
            return $section->getReport()->getTitle() !== $name;
        });

        $this->sections = array_values($filtered);
    }

    public function getLength($maskOffTime = false)
    {
        $length = 0;

        foreach ($this->sections as $section) {
            if ($maskOffTime && $section->getReport()->isOffTime()) {
                continue;
            }
            $length += $section->getLength();
        }

        return $length;
    }

    public function getData($maskOffTime = false)
    {
        $result = [];
        $currentTimePointer = $this->beginning;

        /** @var Section $section */
        foreach ($this->sections as $position => $section) {
            if ($maskOffTime && $section->getReport()->isOffTime()) {
                continue;
            }
            $result[] = [
                $position,
                date('H:i:s', $currentTimePointer),
                date('H:i:s', $currentTimePointer + $section->getLength()),
                $section->getReport()->getTitle(),
                $section->getHumanLength(),
            ];
            $currentTimePointer = $currentTimePointer + $section->getLength();
        }

        return $result;
    }

    public function getDataGrouped($maskOffTime = false)
    {
        $groupedSections = [];

        foreach ($this->sections as $section) {
            if ($maskOffTime && $section->getReport()->isOffTime()) {
                continue;
            }
            if (array_key_exists($section->getReport()->getTitle(), $groupedSections)) {
                /** @var Section $tmpSection */
                $tmpSection = $groupedSections[$section->getReport()->getTitle()];
                $tmpSection->setLength($tmpSection->getLength() + $section->getLength());
                continue;
            }

            $groupedSections[$section->getReport()->getTitle()] = clone $section;
        }

        $result = [];

        /** @var Section $section */
        foreach ($groupedSections as $section) {
            $result[] = [
                $section->getReport()->getTitle(),
                $section->getHumanLength(),
                sprintf('% 3d', number_format($section->getLength() / $this->getLength() * 100, 0)),
            ];
        }

        return $result;
    }

    public function getReportNames()
    {
        $reportNames = [];

        foreach ($this->sections as $section) {
            if ( ! in_array($section->getReport()->getTitle(), $reportNames)) {
                $reportNames[] = $section->getReport()->getTitle();
            }
        }

        return $reportNames;
    }

    public function getHumanLength($maskOffTime = false)
    {
        $dtF = new DateTime('@0');
        $dtT = new DateTime('@' . $this->getLength($maskOffTime));

        return $dtF->diff($dtT)->format('%H:%I:%S');
    }

    public function getUntrackedTimeLength()
    {
        return time() - $this->beginning - $this->getLength();
    }

    /**
     * @param $id
     * @return bool|Section
     */
    public function getSectionById($id)
    {
        if ( ! array_key_exists($id, $this->sections)) {
            return false;
        }

        return $this->sections[$id];
    }

    public function toString()
    {
        return serialize($this);
    }

    public static function create($data)
    {
        return unserialize($data);
    }

    /**
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return (new DateTime())->setTimestamp($this->identifier);
    }

    public function count()
    {
        return count($this->sections);
    }

    /**
     * @param int $beginning
     */
    public function setBeginning($beginning)
    {
        $this->beginning = $beginning;
    }
}
